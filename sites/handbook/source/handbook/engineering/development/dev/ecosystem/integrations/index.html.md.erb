---
layout: handbook-page-toc
title: Ecosystem Integrations Group
description: The Ecosystem Integrations group is responsible for the Integrations category in the Ecosystem stage.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## About

The Integrations group is a part of the [Ecosystem Stage](https://about.gitlab.com/handbook/product/categories/#ecosystem-stage).
We support the product with 3rd party integrations, REST APIs and GraphQL foundational code, and Webhooks.

This page covers processes and information specific to the Integrations group. See also the [Integrations direction page](/direction/ecosystem/integrations/)
and the [features we support per category](/handbook/product/product-categories/features/#ecosystemintegrations-group).

## How to reach us

To get in touch with the Integrations group, it's best to create an
issue in the relevant project (typically [GitLab](https://gitlab.com/gitlab-org/gitlab)) and add the
`~"group::integrations"` label, along with any other appropriate labels. Then,
feel free to ping the relevant Product Manager and/or Engineering Manager.

For more urgent items, feel free to use the Slack Channel (internal): [#g_ecosystem_integrations](https://gitlab.slack.com/archives/g_ecosystem_integrations).

## Backend Group Members

<%= direct_team(manager_role: 'Engineering Manager, Ecosystem:Integrations', role_regexp: /Backend/) %>

## Frontend Group Members

<%= direct_team(manager_role: 'Engineering Manager, Ecosystem:Integrations', role_regexp: /Frontend/) %>

## OKRs

Each quarter we have a series of Objectives and Key Results (OKRs) for our
group. To find the current OKRs for this
[quarter](https://about.gitlab.com/handbook/finance/#fiscal-year), check the
[Integrations Ally page](https://app.ally.io/teams/44487/objectives?tab=0&time_period_id=135093&viewId=330300).

### Active Quarter OKRs

<iframe src="https://app.ally.io/public/PGCaISxZpEhuNEg" class="dashboard-embed" height="1050" width="100%" style="border:none;"></iframe>

## Metrics

<%= partial("handbook/engineering/development/dev/ecosystem/integrations/metrics.erb") %>

## Work

The Product Manager compiles the list of Deliverable and Stretch issues following
the [product prioritization process](/handbook/product/product-processes/#prioritization),
with input from the team, Engineering Managers, and other stakeholders.
The iteration cycle lasts from the 18th of one month until the 17th of the next,
and is identified by the GitLab version set to be released on the 22nd.

### Collaborating with Counterparts

Engineers are encouraged to work as closely as needed with stable counterparts
including our Product Manager. We should be sure to include documentation, UX,
quality engineering, and application security in the planning process.

Quality engineering is included in our workflow via the [Quad Planning Process](/handbook/engineering/quality/quality-engineering/quad-planning/).

### What to work on

<%= partial("handbook/engineering/development/dev/ecosystem/integrations/what_to_work_on.erb") %>

### Issue Development Workflow

In general, we use the standard GitLab [engineering workflow](/handbook/engineering/workflow/).

The easiest way for Engineering Managers, Product Managers, and other stakeholders
to get a high-level overview of the status of all issues in the current milestone,
or all issues assigned to a specific person, is through the [Development issue board](https://gitlab.com/groups/gitlab-org/-/boards/1290820?scope=all&label_name[]=group%3A%3Aintegrations),
which has columns for each of the workflow labels.

As owners of the issues assigned to them, engineers are expected to keep the
workflow labels on their issues up to date, either by manually assigning the new
label, or by dragging the issue from one column on the board to the next.

Once an engineer starts working an issue, they mark it with the `workflow::"in
dev"` label as the starting point and continue [updating the issue throughout development](/handbook/engineering/workflow/#updating-issues-throughout-development).
The process primarily follows the guideline:

``` mermaid
graph LR
  classDef workflowLabel fill:#428BCA,color:#fff;

  A(workflow::in dev):::workflowLabel
  B(workflow::in review):::workflowLabel
  C(workflow::verification):::workflowLabel

  A -- Push an MR --> B
  B -- Merged --> C
  C --> D{Works on production?}
  D -- YES --> Close
  D -- NO --> E[New MR]
  E --> A
```

### Issue Boards

The work for the Integrations group can be tracked on the following issue boards:

#### Workflow Boards

Workflow Boards track the workflow progress of issues.

- [BE Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/1290820?scope=all&utf8=%E2%9C%93&label_name[]=group%3A%3Aintegrations&label_name[]=backend)
- [FE Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/1290820?scope=all&utf8=%E2%9C%93&label_name[]=group%3A%3Aintegrations&label_name[]=frontend)

#### Team Member Boards

Team Member Boards track `group::integrations` labeled issues by the assigned team member.

- [BE Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/2168283?scope=all&label_name[]=backend&label_name[]=group%3A%3Aintegrations)
- [FE Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/2168283?scope=all&label_name[]=frontend&label_name[]=group%3A%3Aintegrations)

#### Deliverable / Stretch Boards

Deliverable Boards tracks `~Deliverable` and `~Stretch` issues.

- [BE Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/412126?label_name[]=backend&label_name[]=group%3A%3Aintegrations)
- [FE Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/412126?label_name[]=frontend&label_name[]=group%3A%3Aintegrations)

### Monitoring

This is a collection of links for monitoring Integrations features.

#### Grafana dashboards

- [Ecosystem:Integrations group dashboard](https://dashboards.gitlab.net/d/stage-groups-integrations/stage-groups-group-dashboard-ecosystem-integrations): Also contains links to various Kibana logs, filtered to our feature categories.
- [Worker queues](https://dashboards.gitlab.net/d/sidekiq-queue-detail/sidekiq-queue-detail?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-queue=jira_connect:jira_connect_sync_branch): Switch queues with the `queue` dropdown.

#### Sentry errors

- [Matching "ServicesController"](https://sentry.gitlab.net/gitlab/gitlabcom/?query=is%3Aunresolved+ServicesController)
- [Matching "Integrations"](https://sentry.gitlab.net/gitlab/gitlabcom/?query=is%3Aunresolved+Integrations)
- [Matching "Jira"](https://sentry.gitlab.net/gitlab/gitlabcom/?query=is%3Aunresolved+Jira)

#### Kibana logs

##### JiraConnect workers

- [`JiraConnect::SyncMergeRequestWorker`](https://log.gprd.gitlab.net/goto/309f97d4a5c3e918e2c07754fefc94ee) errors.
- [`JiraConnect::SyncBranchWorker`](https://log.gprd.gitlab.net/goto/96364e957898896c4dc7e9ee5534b6de) errors.
- [`JiraConnect::SyncProjectWorker`](https://log.gprd.gitlab.net/goto/5f0e03847ddc1b074d6346199c8bc4d2) errors.
- [All JiraConnect sync worker](https://log.gprd.gitlab.net/goto/39348f2d169e6929c41dba2d6fb063ee) timeout errors.

### Capacity Planning

<%= partial("handbook/engineering/development/dev/create/capacity_planning.erb") %>

### Backlog Refinement

<%= partial("handbook/engineering/development/dev/ecosystem/integrations/backlog_refinement.erb") %>

### Retrospectives

<%= partial("handbook/engineering/development/dev/ecosystem/integrations/retrospectives.erb") %>

## Employee Development

Here are some resources team members can use for employee development:

- [Create Stage Professional Development](https://about.gitlab.com/handbook/engineering/development/dev/create/#professional-development)
- [Create Stage Training opportunities](https://about.gitlab.com/handbook/engineering/development/dev/create/engineers/training/)
- [GitLab Learning and Development](https://about.gitlab.com/handbook/people-group/learning-and-development/)
- [GitLab Learn](https://gitlab.edcast.com/)
- [Ecosystem Career Chats](/handbook/engineering/development/dev/ecosystem/integrations/career-chats)
